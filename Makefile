DEPS = function.h
LIB_OBJS = function.o

# https://stackoverflow.com/questions/18007326/how-to-change-default-values-of-variables-like-cc-in-makefile
ifeq ($(origin CC),default) 
	CC = gcc
endif

.PHONY: all
all: main_static main_argv main_argv_fixed-stdin  main_fuzzing-loop main_shared-lib_fuzzing-loop shared-lib_fuzzing-loop.so
	@echo All done!

all-afl: all 

	@echo All+AFL++ done!

main_static: $(LIB_OBJS) main_static.o
	$(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS)

main_argv: $(LIB_OBJS) main_argv.o
	$(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS)

main_argv_fixed-stdin: $(LIB_OBJS) main_argv_fixed-stdin.o
	$(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS)

main_fuzzing-loop: $(LIB_OBJS) main_fuzzing-loop.o
	$(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS)

main_shared-lib_fuzzing-loop: $(LIB_OBJS) main_shared-lib_fuzzing-loop.o 
	$(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS) -ldl

shared-lib_fuzzing-loop.so: $(LIB_OBJS) shared-lib_fuzzing-loop.o
	$(CC) $(LDFLAGS) $^ --shared -fPIC -o $@ $(LDLIBS)

function.o:
	$(CC) -c -fPIC $(CFLAGS) function.c

shared-lib_fuzzing-loop.o:
	$(CC) -c -fPIC $(CFLAGS) shared-lib_fuzzing-loop.c


%.o: %.cpp $(DEPS)
	$(CC) $(CFLAGS) $<

.PHONY: clean
clean:
	rm -f *.o main_static main_argv main_argv_fixed-stdin main_fuzzing-loop main_shared-lib_fuzzing-loop shared-lib_fuzzing-loop.so 
	@echo Clean done!
