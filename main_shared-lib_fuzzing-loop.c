/*
   Copyright (C) 2020 Nikolay Shaplov (dhyan@nataraj.su)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>


#pragma clang optimize off
#pragma GCC            optimize("O0")
int main(int argc, char *argv[])
{
  volatile char dummy[] =  "##SIG_AFL_PERSISTENT##";
  void* lib_handle = NULL;
  void (*shared_lib_main) (char*);  // Pointer to a function from the lib
//  FILE *f;

  if (argc < 2)
  {
    fprintf(stderr,"Please specify input file name as a first arg\n");
    exit(1);
  }

  printf("Loading a .so library...\n");

//  lib_handle = dlopen( "/home/nataraj/dev/fuzzing-examples/fuzzing-examples/shared-lib_fuzzing-loop.so", RTLD_LAZY );
 lib_handle = dlopen( "./shared-lib_fuzzing-loop.so", RTLD_LAZY );

  if( !lib_handle )
  {
    fprintf( stderr, "ERROR: dlopen() %s\n", dlerror() );
    exit( 1 );
  }
  shared_lib_main = (void (*)(char*)) dlsym( lib_handle, "shared_lib_main" );

/*  f = fopen("./aaa","a");
  fprintf(f,"%s - from main\n",argv[1]);
  fclose(f);*/
  ( *shared_lib_main )(argv[1]);

  exit(0);
}
