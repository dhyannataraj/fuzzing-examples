/*
   Copyright (C) 2020 Nikolay Shaplov (dhyan@nataraj.su)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/

#include "function.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[])
{
  OurInt * res;
  OurInt * src_from_file;
  int i;
  char keybuf[100];
  char c;
  char kb_pos=0;

  if (argc < 2)
  {
    fprintf(stderr,"Please specify input file name as a first arg\n");
    exit(1);
  }

  /* It is the way it is done in postgres single-mode console */
  printf("Are you sure? You really want to run it? Type 'yes' and press Enter to confirm:\n");
  while ((c = getc(stdin)) != EOF)
  {
    keybuf[kb_pos] = c;
    if (c=='\n') break;
    kb_pos++;
    if (kb_pos>=sizeof(keybuf))
    {
      fprintf(stderr,"Key bufer overflow\n");
      exit(1);
    }
  }
  keybuf[kb_pos]='\0';
  if (strcmp(keybuf,"yes"))
  {
    printf("Exiting\n");
    exit(0);
  }

  printf("Loafing file '%s'...\n",argv[1]);

  src_from_file = read_data_from_file(argv[1]);

  res = function_to_fuzz(src_from_file);
  for (i=0; i<BUFSIZE; i++)
  {
     printf("res[%d]=%d\n",i,res[i]);
  }
  free(res);
  free(src_from_file);
}
