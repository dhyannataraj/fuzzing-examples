/*
   Copyright (C) 2020 Nikolay Shaplov (dhyan@nataraj.su)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/

#include "function.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
  OurInt * res;
  OurInt * src_from_file;
  int i;


  if (argc < 2)
  {
    fprintf(stderr,"Please specify input file name as a first arg\n");
    exit(1);
  }
  printf("Loafing file '%s'...\n",argv[1]);

  src_from_file = read_data_from_file(argv[1]);

  /*
     Function tangle_pathes_from_file, when built with CFLAGS=-O0, creates
     excessive pathes. Uncomment line below to see how fuzzer will behave
     when have some code with unpredictable pathes before trarget function
     in standalone fuzzing
  */

/*  printf("Tangle path seed: 0x%0llX\n", tangle_pathes_from_file(argv[1])); */


  res = function_to_fuzz(src_from_file);
  for (i=0; i<BUFSIZE; i++)
  {
     printf("res[%d]=%d\n",i,res[i]);
  }
  free(res);
  free(src_from_file);
}
