/*
   Copyright (C) 2020 Nikolay Shaplov (dhyan@nataraj.su)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/

#include "function.h"
#include <stdio.h>
#include <stdlib.h>

/* Do fuzzer detection, and define proper FUZZER_LOOP */
#ifdef __AFL_LOOP
#  define __FUZZER_LOOP __AFL_LOOP
#endif

#ifdef __ISP_LOOP
#  define __FUZZER_LOOP __ISP_LOOP
#endif

/* If we do not use a fuzzer, this will make FUZZER_LOOP run only once */
#ifndef __FUZZER_LOOP
   int stub_fuzzing_loop_condition = 0;
#  define __FUZZER_LOOP(N) ( stub_fuzzing_loop_condition= ! stub_fuzzing_loop_condition )
#endif



int main (int argc, char *argv[])
{
  OurInt * res = NULL;
  OurInt * src_from_file = NULL;
  int i,j,size;
  FILE *f;

  if (argc < 2)
  {
    fprintf(stderr,"Please specify input file name as a first arg\n");
    exit(1);
  }
  printf("Loafing file '%s'...\n",argv[1]);

  /*
     Function tangle_pathes_from_file, when built with CFLAGS=-O0, creates
     excessive pathes. Uncomment line below to see how fuzzer will behave
     when have some code with unpredictable pathes before FUZZING_LOOP
  */

/*  printf("Tangle path seed: 0x%0llX\n", tangle_pathes_from_file(argv[1])); */


  i=0;
  while (__FUZZER_LOOP(16))
  {
    /* Раскомментируйте ниже для тестового вывода мутированных данных */
/*    char name[128];
    sprintf(name, "indata_%i",i);
    dump_on_read = name;*/

    if (res) free(res);
    if (src_from_file) free(src_from_file);

    src_from_file = read_data_from_file(argv[1]);

    /* Раскомментируйте ниже для тестового вывода мутированных данных */
/*    dump_on_read = NULL;*/

    res = function_to_fuzz(src_from_file);
    i++;
  }

  for (i=0; i<BUFSIZE; i++)
  {
     printf("res[%d]=%d\n",i,res[i]);
  }
  free(res);
  free(src_from_file);
}
