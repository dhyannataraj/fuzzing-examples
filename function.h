/*
   Copyright (C) 2020 Nikolay Shaplov (dhyan@nataraj.su)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/

#define BUFSIZE 25
#define OurInt int
#define TERMINATOR INT_MIN
#define OUT_INT_MAX INT_MAX

extern char* dump_on_read;

OurInt * function_to_fuzz(OurInt* in);
OurInt* read_data_from_file(char* name);

unsigned long long int tangle_pathes(unsigned long long int var);
unsigned long long int tangle_pathes_from_file(char * filename);

