# This is a sample for exploring fuzzing process

Before fuzzing real things, it is better to try it with some artificial, yet non-trivial example.

These examples run fuzzing for `function_to_fuzz` function in different modes. `function_to_fuzz` do some 'real' data
processing, and has predefined non-trivial bugs in it. Fuzzer should find them.


## What does `function_to_fuzz` do

`function_to_fuzz` takes input data, an array of integers, process it, and returns output data, also an array of integer. So what does it do

* `function_to_fuzz` fills the 'buffer'. Buffer size is BUFSIZE, from the beggining it is filled with zeros;
* There is a 'cursor' that points to the one of the elements of the 'buffer'. It is moved while data processing. Processing starts with 'cursor' pointing at the first element of the 'buffer';
* Input data has set of 'instructions';
* Each instruction is a pair of integers, first integer is value specifying how far 'cursor' should be moved forward (positive values) of backward (negative values),
  second integer would be added to the 'buffer' value at the point, where 'cursor' was moved;
* First integer of input data is number of 'instructions' in the dataset, and the rest integers are pairs of 'instruction' integers.
* When `function_to_fuzz` is out of instructions, the cursor is moved one position right, and then next three elements are filled with `TERMINATOR` values

`function_to_fuzz` returns this filled `buffer` as output data

As you can see, you theoretically can easily have input data that will cause writing at the positions out of `buffer`.
Some of such cases are protected by proper Asserts:

* it will assert if cursor is moved before the head of the buffer: `assert(cursor >= 0);`
* it will assert if cursor is moved beyond the end of the buffer: `assert(cursor < BUFSIZE);`

But some cases is not properly asserted:

* When buffer is filled with three `TERMINATOR`, it does not respect BUFSIZE and can be written beyond buffer end. This should be caught by ASan.
* When value is added to the `buffer` element, there is no check for integer overflow. This should be caught by UBSan.

Hopefully if everything goes right, fuzzer should find these two problems. And practically it do it, and quite fast.

# Same thing in Russian:

#Это пример для эксперементов с фаззингом

Перед тем как переходить к чему-то более сертезному, есть смысл потренироваться на кошечах, например на этом примере.

Исследуемой функцией тут у нас является функция `function_to_fuzz`. В нее заложены не очень тривиально обнаруживаемые баги.

Смысл функции в следующем:

Функция применяет некий набор правил к некоему буферу.

 * Буфер размера BUFSIZE и изначально заполнен нулями;
 * Есть "курсор" перемещающийся по буферу;
 * В начале работы "курсор" указывает на нулевой элемент буфера;
 * Правило представляет сосбой пару целых чисел. Первое число указывает на сколько позиций следует сместить "курсор", второе
   говорит на какое значение надо увеличить значение элемента буфера на которое будет указывать "курсор" после перемещения.
 * function_to_fuzz принимает в качестве параметров указатель на целочисленный массив, нулевой элемент этого массива содержит
   колличество правил в массиве, все следующие за ним элементы сами правила.
 * После применения последнего правила, функция function_to_fuzz заполняет три элемента следующие за курсором значением TERMINATOR

Функция штатно отрабатывает следующие исключения:

 * Курсор вышел за пределы буфера влево;
 * Курсор вышел за пределы буфера вправо;

Следующая проблема считается не исправимой:

 * Неверно переданное значение количества правил может привести к segmentation fault но изнутри исследуемой функции
   никак не может быть проверено. Поэтому предметом фаззинг-исследования она не является.

Заведомо заложенные проблемы, для обнаружения фаззером:

 * Выход за пределы буфера при заполнении терминирующими символами. Если после применения правил курсор оказался рядом с концом
   буфера, просходит запись за пределы буфера. Проверки выхода за пределы тут нет.
 * Целочисленное переполнение элемента буфера. В элемент можно надобавлять больше чем позволяет размер его элемента. Это
   поведение должен поймать UbSan.

## Разные варианты исполняемых файлов

Сама функция `function_to_fuzz` находится в файле `function.c`. Кроме в прокете есть того серия файлов `main_*.c` применяющая
эту функцию в разных режимах.

### main_static.c

Входные данные для функции `function_to_fuzz` определены в коде программы. Это удобно для разработки, но не удобно для фаззинга.

### main_argv.c

Программа `main_argv` принимает в качестве аргумента имя фала содержащие входные данные для функции `function_to_fuzz`. Эту программу
уже гораздо удобнее использовать как фаззинг-цель. Тогда фаззер может передавать разные смутированные данные через аргументы
командной строки

### TODO

Еще надо будет сделать вариант исполняемого файла с врезанным внутрь Fuzzing Loop'ом.

## Примеры входных данных

В директории `in_samples` лежат разные версии входных данных, затрагивающих разные аспекты работы функции `function_to_fuzz`.

### normal.bin
```
OurInt src[] = {3, 15,30, -10,-20, 3,6};
```
Входные данные для демонстрации  штатной работы.

### assert_left.bin
```
OurInt src[] = {3, 15,30, -30,-20, 3,6};
```
Данные вызывающие при применении "правил" выход за пределы буфера влево, и
вызывающие соответствующий Assert

### assert_right.bin
```
OurInt src[] = {3, 15,30, 20,-20, 3,6};
```
Данные вызывающие при применении "правил" выход за пределы буфера вправо, и
вызывающие соответствующий Assert

### seg_fault.bin
```
OurInt src[] = {3, 15,30, 8,-20, 0,6};
```
Данные вызывающее выход за пределы буфера в право, при заполнении тремя символами
TERMINATOR. Этот выход Assert'ами не обвешан, должен вызывать Segmentation Fault если повезет
или срабатывание санитайзеров, если с ними собрано.

### overflow.bin
```
OurInt src[] = {3, 15,0x4fffffff, 0,0x4fffffff, 0,6};
```
Данные вызывающие целочисленное переполнение: к переменной типа OurInt суммарно прибавляется
больше чем в нее может влезть. Должно вызвать срабатывание UbSan'а

### Создание новых файлов
```
include "function.h";
OurInt src[] = {3, 15,100, 0,100, 0,6};

int main()
{
  f = fopen("in_samples/new_sample.bin","wb");
  fwrite(src,sizeof(src),1,f);
  fclose(f);
}
```
