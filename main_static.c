/*
   Copyright (C) 2020 Nikolay Shaplov (dhyan@nataraj.su)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/


#include "function.h"
#include <stdio.h>
#include <stdlib.h>


OurInt src[] = {3, 15,30, -10,-20, 3,6};     // Нормально работающие данные

//OurInt src[] = {3, 15,30, -30,-20, 3,6};  // Assert из-за вылеза влево
//OurInt src[] = {3, 15,30, 20,-20, 3,6};  // Assert из-за вылеза вправо
//OurInt src[] = {3, 15,30, 8,-20, 0,6}; // Записть за пределы буфера (при дополнении терминаторами)
//OurInt src[] = {3, 15,0x4fffffff, 0,0x4fffffff, 0,6}; // Целочисленное переполнение


int main(void)
{
  OurInt * res;
  int i;

  res = function_to_fuzz(src);
  for (i=0; i<BUFSIZE; i++)
  {
     printf("res[%d]=%d\n",i,res[i]);
  }
  free(res);
}
